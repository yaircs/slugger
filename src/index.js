export default function slugger(...args) {
    if(args.length === 0) {
        throw new Error('slugger need to be given at least one string')
    }
    let resString = "";
    [...args].forEach(arg => {
        if (typeof arg !== 'string') {
            throw new Error('slugger accepts only strings as arguments');
        }
        if (arg === arg.replaceAll(' ', '-')) {
            resString += arg+'-';
        }
        else {
            resString += arg.replaceAll(' ', '-')+'-';
        }
    })
    resString = resString.slice(0, resString.length-1);
    return resString;
}



import slugger from './index.js';
import log from '@ajar/marker';

const input = ["this is a sentence", "second sentence","word1", "word2", "also a sentence", "word3"];

log.yellow('input: ', ...input);
log.blue('output: ', slugger(...input));

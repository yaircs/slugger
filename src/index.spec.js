import slugger from './index.js'
/**
 * @describe [optional] - group of tests with a header to describe them
 */
describe('testing slugger basic functionality', () => {
    it("should throw with no params", () => {
        expect(() => {
            slugger()
        }).toThrowError(
            'slugger need to be given at least one string'
        );
      });

      it("should throw with wrong params", () => {
        expect(() => {
            slugger(2)
        }).toThrowError(
          'slugger accepts only strings as arguments'
        );
      });
    /**
     * @it - unit tests can use the 'it' syntax
     */
    it('slugger can slug string with spaces', () => {
        expect(slugger('slugger can slug string with spaces')).toEqual('slugger-can-slug-string-with-spaces');
    })
    /**
     * @test - unit test can use the 'test' syntax
    //  */
    test('slugger can slug any number of spacy strings', () => {
        expect(slugger('slugger can slug string with spaces', 'also', 'this', 'and this')).toEqual('slugger-can-slug-string-with-spaces-also-this-and-this');
    })
})

